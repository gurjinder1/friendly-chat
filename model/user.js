var express = require ( "express" ),

	firebase = require('firebase');

	require('firebase/auth');

	require ("firebase/database");

var fs = require ("file-system")

var firebaseTokenGenerator = require ("firebase-token-generator")

var tokenGenerator = new firebaseTokenGenerator("QaVSg648ZN7x6qnSCTCVRFv3Ye7NZ4qXJtsJYthc");

var config = require ("privateFiles/firebaseConfig")

var imageUploadingConfig = require ("privateFiles/imageConfig")

var gcloud = require('google-cloud')(imageUploadingConfig);


var admin = require("firebase-admin");

admin.initializeApp({
  credential: admin.credential.cert("privateFiles/friendly-chat-44d47-firebase-adminsdk-i2nt2-d99039ac13.json"),
  databaseURL: "https://friendly-chat-44d47.firebaseio.com"
});

var gcs = gcloud.storage();

var app = firebase.initializeApp(config);

var dataRef = app.database().ref();

var userAuth = app.auth();

var user = function user(){};

user.prototype.userState = function checkUserState(done)
{
	done( userAuth.currentUser );
};	

user.prototype.signInWithUserNameAndPassword = function signInWithUserNameAndPassword(email, password)
{
	return userAuth.signInWithEmailAndPassword (email, password );
}

user.prototype.createUser = function createuser (password, details, done)
{
//	console.log (details);

	userAuth.createUserWithEmailAndPassword (details.email, password)

	.then (function (user)
	{
		//console.log(user)

		var userRef = dataRef.child ("user").child (user.uid)
		
		return userRef.set(details);
	})
	.then (function ()
	{
		console.log ("ok")
		done (null, details)
	})
	.catch (function( error)
	{
		done (error)
	})
}

user.prototype.setPicture = function (file, done)
{
	this.uploadPhoto(file)
	
	.then(function (url)
	{
		var updatePicRef = dataRef.child ("user").child(userAuth.currentUser.uid);
		updatePicRef.child("picture").update(url)
		.then (function (status)
		{
			done (null, status)			
		})
		.catch (function (error)
		{
			done(error)
		})
	})
	.catch (function (error)
	{
		done(error)
	})
}

user.prototype.uploadPhoto = function (data)
{
	return new Promise(function(resolve, reject)
	{
	 		var backups = gcs.bucket("friendly-chat-44d47.appspot.com")

	 		var option  = 
	 		{
	 			destination: "public/" + data.filename
			}
			console.log (data.path)
			backups.upload(data.path, option  , function(err, file)
			{
				fs.unlink(data.path, (error) => {
			  		if (error || err)
			  		{
			  			reject ( error || err);
			  			return;
			 		}
			 		else
			 		{
			 			var file = backups.file(data.path);
			 			file.getMetadata(function(err, metadata, apiResponse) 
			 			{
  							if (err) {
    							console.log(err);
  							} else {
    						console.log (metadata.downloadURL)
    						console.log (apiResponse)
  						}
					});

			 		}
				})
			})	
	})
}

user.prototype.getMutualFriends = function( uid,done)
{
	var userList = dataRef.child("user");
	userList.on("value", snap =>{
		var friendList = snap.val();
		
		var currentUser = uid;
		
		delete friendList[currentUser];
				
		done (null, friendList);
	})

}

user.prototype.createToken = function(done) {
	// token =tokenGenerator.createToken({uid: userAuth.currentUser.uid});
	//  console.log (token)
	var token = admin.auth().createCustomToken(userAuth.currentUser.uid); 
	done (token);

}


user.prototype.getTokens = function() 
{
	return (userAuth.currentUser.getToken(true))
};

user.prototype.verifyIdToken = function(idToken) 
{
	
	return admin.auth().verifyIdToken(idToken)

};
user.prototype.setMessage = function ( uid, message)
{
	message.date = Date.now();
	
	var currentUser = uid;;

	var userObject = {};
		
	var chatRef = dataRef.child("chat/" + concatUserId (currentUser, message.friendId)+"/" )

	message.sender = currentUser;

	return chatRef.push(message);
}

user.prototype.getMessage = function(sender, reciever, send) {
	var friendChat = concatUserId(sender, reciever);
	console.log (friendChat)
	var messageRef = dataRef.child ("chat/"+friendChat).limitToLast(10);
	messageRef.once("value", snap =>{
		var messages = snap.val();		
		console.log("ok");
		send (null, messages);
	})
};

user.prototype.logout = function ()
{
	return userAuth.signOut();
}

function concatUserId (user1, user2)
{
	return user1 > user2 ? user2 + "-" + user1 : user1+"-"+user2;
}

module.exports = user;