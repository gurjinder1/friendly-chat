var express = require ("express");;

var user = require ("controller/chat.js");

var bodyParser = require ("body-parser")

var multer = require ("multer");

var cookieParser = require ("cookie-parser")

var app = express ();

var storage =   multer.diskStorage(
{  
  destination: function (req, file, callback) {
    callback(null, 'public');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now()+'.jpeg');
  }
});


var upload = multer({ storage : storage });

var router = express.Router();

//app.use(expressSession);

//app.use(app.router);

router.get( '/', user.login);

router.get("/profile", user.profile);

router.get("/reset", user.resetSession);

router.get( "/getUID", user.getSession )

router.post("/login",  bodyParser.urlencoded({extended : true}), user.loginAuth)

router.post ("/register", bodyParser.urlencoded({extended : true}), user.register);

router.post("/picUpdate" , upload.single( "profilePic" ) , user.updatePic)

router.post ("/getMessage", user.getMessages);

router.get("/otherUser" , user.known)

router.post ("/setUID" , user.setUID)

router.get ("/v", user.verifyToken);

router.get("/logout", user.logout)

module.exports = router;


