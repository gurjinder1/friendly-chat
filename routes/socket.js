var socketio = require('socket.io');
var db = require('model/user');
var io;

var userManagement = new db();

var nicknames = {};
var clients = [];
var namesUsed = [];

exports.listen = function(server){
  io = socketio.listen(server);
  io.set('log level', 1);

  io.sockets.on('connection', function(socket){
  
  		//intializeMessage (socket);
      socket.on('join', function (data) {
          console.log (data)
          socket.join(data.userId); // We are using room of socket io
      });

      socket.on("send-message", function (message)
  		{
          userManagement.setMessage(message.userId , {friendId : message.friend , message : message.message})
          .then(function (status){
  				    console.log (message)
              io.sockets.in(message.friend).emit('new-massage', message);
              io.sockets.in(message.userId).emit("new-massage", message);
                    //io.sockets.emit("new-massage", message);  // socket.send (stringify (message))
          })	
  		})

      socket.on ("get-previos-message", function (users)
      {
          userManagement.getMessage (users.sender, users.reciever,function(err, message)
          {
              if(err)
              {
                io.sockets.emit ("previos-message", {})
              }
              else 
              {
                io.sockets.emit ("previos-message", message)
              }
          })
      })  
  });

}

