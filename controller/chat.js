var UserManagement = require ("model/user");

var userManagement = new UserManagement();

var user = {};


var tokens = "";
user.profile = function(request, response)
{
	console.log (request.session)
	var user = request.session
	if (user && user.userID)
	{

		response.render ("index.ejs");
	}
	else
	{
		response.redirect ("/");
	}		
 	
}

user.getSession = function (request, response)
{
	response.status(200).send ({uid:request.session.userID})
}

user.login = function (request, response)
{
//	console.log (request.session)
	var user = request.session
	if (!user.userID)
	{
		response.render ("login.ejs");
	}
	else
	{
		response.redirect ("/profile");
	}		
}

user.getMessages = function (request, response)
{
	extractObject(request, function (friends)
	{
		var currentUser = request.session.userID;
		var friend = friends.friendId
		userManagement.getMessage (currentUser, friend, function(err, message)
	    {
	        if(err)
	        {
	            response.status (400).send("previos-message", {})
	    	}
	      	else 
	      	{
	      		//console.log (message)
	        	response.status(200).send (message)
	      	}
	  	})	
	})
	
}

user.verifyToken = function ()
{
	console.log (request.session)

	userManagement.verifyIdToken(tokens)
	.then (verifiedToken => {
		console.log (verifiedToken)
	})
	.catch (error => {
		console.log (error);
	})

}

user.loginAuth = function (request, response)
{
	var user = request.body.username;
	var password = request.body.password;

	userManagement.signInWithUserNameAndPassword(user, password)

	.then(function (user)
	{
		userManagement.createToken(function (token)
		{
			tokens = token;
			console.log (token)
//			console.log (userManagement.getTokens() );

			response.redirect ("/profile")
		});
		
	})
	.catch(function (error)
	{
		console.log (error)
		response.redirect("/");
	})

}

user.register = function (request, response)
{	
	var clientInfo = 
	{
		firstName: request.body["first-name"],
		lastName : request.body["last-name"],
		email : request.body["email"],
		picture : "default.jpg"
	};

	var password = request.body["password"]
	var confirm = request.body["c-password"]
	

	if (password === confirm)
	{
		userManagement.createUser ( password, clientInfo , function(err, user)
		{
			if(err)
			{
				console.log(err)
				response.redirect ("/")
			}
			else
				response.render  ("upload.ejs");
		})
	}
}

user.updatePic = function (request, response)
{
	userManagement.setPicture( request.file, function(err, path)
	{
		if(err)
		{
			console.log (err)
			response.redirect ('/')
			return
		}
		console.log (path)
		response.redirect("/profile");
	})
}

user.logout = function (request, response)
{
	userManagement.logout ()
	.then (function (accept)
	{
		response.status(200).redirect("/");
	})
}

user.known = function (request, response)
{
	console.log (request.session)
	var user = request.session;
	if (user && user.userID)
	{
		userManagement.getMutualFriends(user.userID, function(error ,friends )
		{
			if(error)
			{
				response.status(400).send(error)
				return;
			}
			response.status(200).end(JSON.stringify(friends));
		})
	}
}

user.setUID = function (request, response)
{
	extractObject (request, function (user)
	{
		if(user && user.uid)
		{
			request.session.userID = user.uid;
			console.log (request.session)
			response.status (200). send ( {status:"ok"} )
		}
		else 
		{
			response.status(400).send ({error : "uid is not defined"})
		}
	})
}

user.resetSession = function(request, response)
{
	if (request.session.userID) 
	{
        request.session.destroy(function() 
        {
	    	response.status(200).send(JSON.stringify ({sucess :"sucessfully sign out"}))
	    });
    }
    else 
    {
    	response.status (400).send (JSON.stringify({failed : "already signOut"}))
    }
}

function extractObject (request, callback)
{
	var body = "";
	var post = "";
	 request.on('data', function (data) {
            body += data;
      });

     request.on('end', function () {
            post = JSON.parse(body);
              callback (post)

      });
   
}


module.exports = user