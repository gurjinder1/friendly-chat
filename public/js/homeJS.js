var msg = document.getElementById("chat");
function Chat ()
{
	this.database = new Database();
	this.getFriendList.apply(this);
	this.socket = io.connect()
	this.currentlyChatFriend = "";
	this.joinChat.apply(this);
	this.msg = document.getElementById("chat");
	this.users = document.getElementById("users")
	this.messageList = document.getElementById("prev-message");
	this.messageBody = document.getElementById( "message-body" )
	this.newMessage = document.getElementById("message")
	this.messageBox = document.getElementById("message-box")
	this.sendMessage = document.getElementById("submit")
	this.logout = document.getElementById("logout");
	this.friendName = document.getElementById("name-of-friend");
 	this.sendMessage.addEventListener ("click", this.sendMessageToNewUser.bind(this));
	this.loadMessage.apply(this);
	this.logout.addEventListener("click", this.logoutRedirect.bind(this));
}

Chat.prototype.joinChat = function()
{
	var thisRef = this;
	sendRequest ("getUID" , "get", function (error, uid)
	{
		if (error)
		{
			console.log (error)
			return;
		}
		console.log (uid)
		thisRef.socket.emit('join', {userId: uid.uid});

	})

}

Chat.prototype.logoutRedirect = function() {
	this.database.logout()
	.then (status =>{
		sendRequest("/reset","get",function(error , status)
		{
			location.href = "/";			
		})
	})
	.catch(error =>{
		location.href = "/profile"
	})
};

Chat.prototype.sendMessageToNewUser = function (event)
{
	event.preventDefault();
	this.current = this.database.getUserID();

	this.socket.emit("send-message", {userId : this.database.getUserID(), friend : this.currentlyChatFriend, message : this.newMessage.value})
	this.newMessage.value = ''
}

Chat.prototype.loadMessage = function ()
{
	var thisRef = this;
	this.socket.on ("new-massage", function(message)
	{


		var newDiv = document.createElement("div")

		newDiv.innerHTML ="<ul class = 'chat_message'><li>"+message.message+"</li></ul>";
		
		if (message.userId !== thisRef.currentlyChatFriend)
			newDiv.setAttribute ("class", "chat_message_wrapper chat_message_right")
		else 
			newDiv.setAttribute ("class", "chat_message_wrapper ")

		thisRef.messageList.appendChild(newDiv);
		scrollToBottom (msg)		
	})
}

Chat.prototype.getFriendList = function()
{
	thisRef = this;

	sendRequest("/otherUser", "get", function(error,response)
	{

		for(key in response)
		{
			var newDiv = document.createElement ("div");
			newDiv.setAttribute("class", "round hollow text-center")

			var anc = document.createElement("a")
			anc.href = "#";
			anc.id = key;
			anc.setAttribute("class", "open-btn")

			anc.innerHTML = response[key].firstName;
			newDiv.appendChild(anc)
			thisRef.users.appendChild(newDiv);
			//breakLine(thisRef.users)	
		}
		thisRef.users.onclick = thisRef.showMessageTab.bind(thisRef)
	})
}

Chat.prototype.showMessageTab = function(event)
{
	var thisRef = this
	var clickElement = event.target;
	if (clickElement.tagName === "A")
	{
		$('#sidebar_secondary').addClass('popup-box-on');
		this.friendName.innerHTML = clickElement.innerHTML;
		this.currentlyChatFriend = clickElement.id;
		console.log (this.currentlyChatFriend);;
		sendRequest("/getMessage", "post", JSON.stringify({friendId : this.currentlyChatFriend}),
				
			function(error, messages)
			{

				console.log (error,messages)
				if (error)
				{
					console.log (error)
				}		
				else
				{
					thisRef.addToDom(messages, thisRef.currentlyChatFriend);
				}
			
			}
		)
	}
}

Chat.prototype.addToDom = function(messages, friend) 
{
	this.messageList.innerHTML = "";
	addMessage(messages, friend, this.messageList);
	scrollToBottom (msg)
};

var chat = new Chat();


function addMessage (message ,friend, div)
{

	for (key in message)
	{
		var newDiv = document.createElement("div")

		newDiv.innerHTML ="<ul class = 'chat_message'><li>"+message[key].message+"</li></ul>";
		if (message[key].sender !== friend)
				newDiv.setAttribute ("class", "chat_message_wrapper chat_message_right")
		else 
				newDiv.setAttribute ("class", "chat_message_wrapper ")

		div.appendChild(newDiv);
	}
}

function sendRequest (url, requestMethod, extraInfo, response)
{
	var request=new XMLHttpRequest();
	request.open(requestMethod, url);
	if(extraInfo instanceof Function )
		request.send();
	else
		request.send(extraInfo);

	request.addEventListener('load',function(event)
	{
		if(request.status == 200)
		{
			var data = ""
			if(request.responseText)
			 data = JSON.parse ( request.responseText )

			if(extraInfo instanceof Function )
				extraInfo(null, data);
			else
				response(null, data);

		}
		else
		{
			var data = JSON.parse ( request.responseText )
			if(extraInfo instanceof Function )
				extraInfo(data);
			else
				response(data);

		}

	});
}


function breakLine(element)
{
	var br = document.createElement ("br")
	element.appendChild(br);
}

function scrollToBottom(e) {
	console.log (e)
e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
}

function show(element)
{

	element.removeAttribute("hidden")
}

function hide( element)
{
	element.setAttribute("hidden", "true")
}