require ('app-module-path').addPath (__dirname );

var http = require('http');

var express = require ("express");

var path = require ("path");

var chat = require ("routes/chat");

var app = new express();

var expressSession = require ('express-session')({  
	secret: 'keyboard cat'
});


var chatServer = require('routes/socket');
app.set("port", process.env.PORT || 3000);

app.set ("view_engine", "ejs");

app.set("views", path.join (__dirname, "view"));

//app.use(app.router);
app.use(expressSession);


app.use("/", chat);

app.use (express.static(path.join (__dirname,"public")));

var server = http.createServer(app).listen(app.get("port")); 

console.log ("listen on 3000")

chatServer.listen(server);

